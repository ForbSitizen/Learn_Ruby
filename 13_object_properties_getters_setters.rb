class Item

  def initialize
    @price = 90
  end

  def price
    @price
  end

  def price=(price_value)
    @price = price_value
  end

end

item01 = Item.new
puts item01.price

item01.price = 100

puts item01.price
