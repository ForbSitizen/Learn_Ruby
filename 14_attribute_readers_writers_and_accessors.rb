class Item

  def initialize
    @price = 90
  end

  attr_accessor :price, :weight

end

item01 = Item.new
#puts item01.price

item01.price = 100
item01.weight = 100

puts "Item's price is #{item01.price}"
puts "Item's weight is #{item01.weight}"
