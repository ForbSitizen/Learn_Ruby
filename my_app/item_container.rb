module ItemContainer

  def add_item(item)
    @items.push(item)
  end

  def remove_item
    @items.shift
  end

  def validate
    @items.each { |i| puts "Item has no price" if i.price.nil? }
  end

  def delete_invalid_item
    @items.delete_if { |i| i.price.nil? }
  end

end