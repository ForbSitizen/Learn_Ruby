require_relative "item_container"
require_relative "item"
require_relative "real_item"
require_relative "virtual_item"
require_relative "cart"
require_relative "order"


item_01 = VirtualItem.new({ :name => "Car", :price => 1000, :weight => 1500 })
item_02 = RealItem.new({ :name => "Bus", :weight => 1500 })
item_03 = RealItem.new({ :name => "Plane", :weight => 2000 })


cart = Cart.new

cart.add_item item_01
cart.add_item item_02

cart.remove_item

puts cart.items.size


order = Order.new

order.add_item item_01
order.add_item item_02
order.add_item item_03

order.remove_item

puts order.items.size