puts 2 + 3 * 5
puts (2 + 3) * 5

# operator "and" - &&
#puts true && true
#puts true && false
#puts false && true
#puts false && false
puts "-------------------"
puts 1 < 2 && 4 > 3
puts 1 > 2 && 4 > 3

#operator "or" ||
puts "-------------------"
puts 1 < 2 || 4 > 3
puts 1 > 2 || 4 > 3
puts 1 < 2 || 4 < 3
puts 1 > 2 || 4 < 3