def my_name(first_name = "", family_name = "")
  greeting = "Hello\n"
  if first_name != "" && family_name != ""
    greeting += "My name is " + first_name + ", " + "my family name is " + family_name
  end
  greeting
end

puts my_name("Forb", "Sitizen")