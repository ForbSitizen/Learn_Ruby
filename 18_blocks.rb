my_data = ["aa", "bb", 123, 456]

my_data.delete_if { |i| i.is_a?(String) }

my_data.each { |i | puts i }

puts "___________________________________________"

data = { :first_name => "Forb", :family_name => "Sitizen" }

data.each_key { |key| puts data[key]}