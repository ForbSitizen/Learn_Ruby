i = 1

if i <= 1
  puts "i less than or equal one"
elsif i >= 1
  puts "i is greater than or equal one"
else
  puts "i is equal one"
end

puts "---------------------------------"

if i <= 1
  puts "i less than or equal one"
end

if i >= 1
  puts "i is greater than or equal one"
end